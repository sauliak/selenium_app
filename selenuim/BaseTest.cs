﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using NUnit.Framework;
using Microsoft.Extensions.Configuration;
using OpenQA.Selenium.DevTools;
using log4net;
using log4net.Config;
using log4net.Repository.Hierarchy;
using System.Xml;

namespace selenuim
{
    public class BaseTest
    {
        protected ChromeDriver driver;
        protected WebDriverWait wait;

        [OneTimeSetUp]
        public void Setup()
        {
            var options = new ChromeOptions();
            var env = Environment.GetEnvironmentVariable("TEST_ENV") ?? "DEV";
            var configProvider = new ConfigProvider(env);
            var baseUrl = configProvider.GetSetting("BaseUrl");
            var invalue = configProvider.GetGenericVal<int>("IntValue");
            //options.AddArgument("--headless");
            //options.AddArgument("--incognito");
            driver = new ChromeDriver(options);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(baseUrl);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            driver.Close();
            driver.Quit();
        }
    }
}
