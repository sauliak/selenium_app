﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using selenuim.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace selenuim.WebParts
{
    internal class LeftMenuWebPart
    {
        private readonly IWebDriver _driver;
        [FindsBy(How = How.Id, Using = "inventory_sidebar_link")]
        private IWebElement AllItems;

        [FindsBy(How = How.Id, Using = "about_sidebar_link")]
        private IWebElement About;

        [FindsBy(How = How.Id, Using = "logout_sidebar_link")]
        private IWebElement LogOut;

        [FindsBy(How = How.Id, Using = "reset_sidebar_link")]
        private IWebElement ResetPage;

        [FindsBy(How = How.Id, Using = "react-burger-cross-btn")]
        private IWebElement CloseMenuBtn;

        public LeftMenuWebPart(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
            _driver = driver;
        }

        public LoginPage ClickLogOut()
        {
            LogOut.Click();
            return new LoginPage(_driver);
        }
    }
}
