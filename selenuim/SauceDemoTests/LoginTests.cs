﻿using NUnit.Framework;
using selenuim.Pages;
using selenuim.Pages.Steps;


namespace selenuim.SauceDemoTests
{
    internal class LoginTests : BaseTest
    {
        private ProductsSteps _productsSteps;
        private ProductsPage _productsPage;
        private LoginSteps _loginSteps;

        [SetUp]
        public void SetUp()
        {
            _loginSteps = new LoginSteps(driver);
            _productsSteps = new ProductsSteps(driver);
            _productsPage = new ProductsPage(driver);
        }

        [Test]
        public void UserCanLoginWithValidCredentials()
        {
            _loginSteps.LoginAs("standard_user", "secret_sauce");
            _productsPage.IsInventoryVisible();
            Assert.IsTrue(_productsPage.IsInventoryVisible(), "Inventory should be visible after successful login.");
        }

        [TestCase("standard_user", "secret_sauce")]
        public void UserCanLoginWithValidCredentials(string login, string password)
        {
            var _productsPage = _loginSteps.LoginAs(login, password);
            _productsPage.IsInventoryVisible();
            Assert.IsTrue(_productsPage.IsInventoryVisible(), "Inventory should be visible after successful login.");
        }

        [Test]
        public void ShouldShowSlepectedProductsInShoppingCard()
        {
            _loginSteps.LoginAs("standard_user", "secret_sauce");
            //_productsSteps.SelectProducsByNameAndContinue("Sauce Labs Backpack", "Sauce Labs Bike Light");
            _productsSteps.Logut();
            Thread.Sleep(5000);
        }
    }
}
