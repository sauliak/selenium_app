﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using selenuim.WebParts;

namespace selenuim.Pages
{
    internal class ProductsPage
    {
        private readonly IWebDriver _driver;

        [FindsBy(How = How.Id, Using = "shopping_cart_container")]
        private IWebElement ShoppingCard;

        [FindsBy(How = How.Id, Using = "react-burger-menu-btn")]
        private IWebElement MenuButton;
 
        // Locator for an element that is only visible after a successful login
        private IWebElement InventoryContainer => _driver.FindElement(By.Id("inventory_container"));

        public ProductsPage(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
            _driver = driver;
        }

        public bool IsInventoryVisible()
        {
            return InventoryContainer.Displayed;
        }

        public IWebElement GetProductByName(string name)
        {
            return _driver.FindElement(By.XPath($"//div[@class='inventory_item' and .//*[contains(text(),'{name}')]]"));
        }

        public IWebElement GetSelectButtnByName(string name)
        {
            return GetProductByName(name).FindElement(By.XPath(".//button"));
        }

        public void ClickShppingCardButton()
        {
            ShoppingCard.Click();
        }

        public LeftMenuWebPart OpenLeftMenu()
        {
            MenuButton.Click();
            return new LeftMenuWebPart(_driver);
        }
    }
}
