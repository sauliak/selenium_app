﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using SeleniumExtras.WaitHelpers;

namespace selenuim.Pages
{
    internal class LoginPage
    {
        private readonly IWebDriver _driver;
        private readonly string _url = "https://www.saucedemo.com/";
        
        [FindsBy(How = How.Id, Using = "user-name")]
        private IWebElement UsernameField;
        
        [FindsBy(How = How.Id, Using = "password")]
        private IWebElement PasswordField;

        [FindsBy(How = How.Id, Using = "login-button")]
        public IWebElement LoginButton;

        [FindsBy(How = How.CssSelector, Using = ".error-message-container.error")]
        private IWebElement ErrorMessage;

        public LoginPage(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
            _driver = driver;
        }

        public void Open()
        {
            _driver.Navigate().GoToUrl(_url);
        }

        public bool IsPresent(By by) {
            bool isPresent;
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            isPresent = _driver.FindElements(by).Count == 0;
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            return isPresent;
        }

        public LoginPage EnterUsername(string username)
        {
            UsernameField.SendKeys(username);
            return this;
        }

        public LoginPage EnterPassword(string password)
        {
            PasswordField.SendKeys(password);
            return this;
        }

        public ProductsPage ClickLogin()
        {
            LoginButton.Click();
            return new ProductsPage(_driver);
        }

        public bool IsErrorMessageDisplayed()
        {
            return ErrorMessage.Displayed;
        }

        public string GetErrorMessage()
        {
            return ErrorMessage.Text;
        }

        public void Login(string username, string password)
        {
            EnterUsername(username);
            EnterPassword(password);
            ClickLogin();
        }
    }
}
