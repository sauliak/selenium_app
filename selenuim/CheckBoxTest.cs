using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace selenuim
{
    public class CheckBoxTest : BaseTest
    {
        By checkBoxSelector = By.XPath("//form[@id='checkboxes']/input[last()]");
        
        [Test]
        public void CheckSecondCheckBoxIsSelectedByDefaultTest()
        {
            driver.Navigate().GoToUrl("https://the-internet.herokuapp.com/checkboxes");
            var checkbox = driver.FindElement(checkBoxSelector);
            Actions action = new Actions(driver);
            Assert.IsTrue(checkbox.Selected);
        }

        [Test]
        public void UnselectCheckBoxTest()
        {
            var lastCheckBox = driver.FindElement(By.XPath("//form[@id='checkboxes']/input[last()]"));
            lastCheckBox.Click();
            Assert.IsFalse(!lastCheckBox.Selected);
            Thread.Sleep(2000);
        }
    }
}