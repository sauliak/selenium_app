﻿using Microsoft.Extensions.Configuration;


namespace selenuim
{
    internal class ConfigProvider
    {
        public IConfigurationRoot Configuration { get; private set; }

        public ConfigProvider(string environmentName = "DEV")
        {
            BuildConfiguration(environmentName);
        }

        private void BuildConfiguration(string environmentName)
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"config.{environmentName}.json", true, false)
                .Build();
        }

        public string GetSetting(string key)
        {
            return Configuration[key];
        }

        public T GetGenericVal<T>(string key)
        {
            return Configuration.GetValue<T>(key);
        }
    }
}
