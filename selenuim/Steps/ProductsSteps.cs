﻿using OpenQA.Selenium;
using selenuim.WebParts;
using selenuim.www.saucedemo.com.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace selenuim.Pages.Steps
{
    internal class ProductsSteps
    {
        private readonly ProductsPage productsPage;
        public readonly LeftMenuWebPart leftMenuWebPart;

        public ProductsSteps(IWebDriver driver)
        {
            this.productsPage = new ProductsPage(driver);
            this.leftMenuWebPart = new LeftMenuWebPart(driver);
        }

        public void SelectProducsByNameAndContinue(params string[] productNames)
        {
            foreach (var productName in productNames)
            {
                var element = productsPage.GetSelectButtnByName(productName);
                element.Click();
            }
            productsPage.ClickShppingCardButton();
        }

        public void Logut() {
            var menu = productsPage.OpenLeftMenu();
            menu.ClickLogOut();
        }
    }
}
