﻿using OpenQA.Selenium;
using selenuim.www.saucedemo.com.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace selenuim.Pages.Steps
{
    internal class LoginSteps
    {
        private readonly LoginPage loginPage;
        private readonly IWebDriver driver;

        public LoginSteps(IWebDriver driver)
        {
            this.loginPage = new LoginPage(driver);
            loginPage.Open();
            this.driver = driver;
        }

        public ProductsPage LoginAs(string username, string password)
        {
            loginPage.EnterUsername(username);
            loginPage.EnterPassword(password);
            loginPage.ClickLogin();
            return new ProductsPage(driver);
        }

        public bool IsOpen()
        {
            return loginPage.LoginButton.Displayed;
        }
    }
}
