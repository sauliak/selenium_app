﻿using OpenQA.Selenium.Interactions;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using NUnit.Framework;

namespace selenuim
{
    internal class HoverImages : BaseTest
    {
        [Test]
        public void TestHoverOverImages()
        {
            // Navigate to the page with the images
            driver.Navigate().GoToUrl("http://the-internet.herokuapp.com/hovers");

            // Find the images by their common class or any other selector that identifies them
            var images = wait.Until(drv => drv.FindElements(By.CssSelector(".figure")));
            int imageIndex = 0;
            // Create an instance of the Actions cl
         
            Actions action = new Actions(driver);
            foreach (var image in images)
            {
                // Perform the hover action
                action.MoveToElement(image).Perform();
                //.figure .figcaption
                var caption = image.FindElement(By.CssSelector(".figcaption"));
                //var captions = image.FindElements(By.XPath("//div[@class='figcaption']")).Count;
                var isDisplayed = caption.Displayed;
                Assert.IsTrue(caption.Displayed, $"Caption for image at index {imageIndex} is not displayed after hover");
                //var caption = wait.Until(drv =>
                //{
                //    try
                //    {
                //        var element = image.FindElement(By.CssSelector(".figcaption"));
                //        return element.Displayed ? element : null;
                //    }
                //    catch (NoSuchElementException)
                //    {
                //        return null;
                //    }
                //});

                // Include the image index in the assertion message
                //Assert.IsTrue(caption.Displayed, $"Caption for image at index {imageIndex} is not displayed after hover");

                imageIndex++; // Increment the counter
            }
        }
    }
}
