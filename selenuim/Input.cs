﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace selenuim
{
    [TestFixture]
    internal class Input : BaseTest
    {
        private IWebElement inputField;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var url = "http://the-internet.herokuapp.com/inputs";
            driver.Navigate().GoToUrl(url);
            inputField = driver.FindElement(By.TagName("input"));
        }

        [SetUp]
        public void SetUp()
        {
            inputField.Clear();
        }

        [Test]
        public void TestInputAcceptsNumericValue()
        {
            inputField.SendKeys("10");
            Assert.AreEqual("10", inputField.GetAttribute("value"), "The input value is not as expected after sending numeric keys.");
        }

        [Test]
        public void TestInputIncreasesValueWithArrowUp()
        {
            inputField.SendKeys("10");
            Assert.AreEqual("11", inputField.GetAttribute("value"), "The input value did not increase after sending arrow up key.");
        }

        [Test]
        public void TestInputDecreasesValueWithArrowDown()
        {
            inputField.SendKeys("10");
            inputField.SendKeys(Keys.ArrowDown);
            Assert.AreEqual("9", inputField.GetAttribute("value"), "The input value did not decrease after sending arrow down key.");
        }

        [Test]
        public void TestInputIgnoresNonNumericValue()
        {
            inputField.SendKeys("10");
            inputField.SendKeys("a");
            Assert.AreEqual("10", inputField.GetAttribute("value"), "The input field should not accept non-numeric values.");
        }
    }
}
